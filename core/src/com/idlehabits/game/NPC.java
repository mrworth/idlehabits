package com.idlehabits.game;

import com.badlogic.gdx.utils.Array;

/**
 * Created by aguac on 4/5/2018.
 */

public class NPC {
    public int id;
    public int level;
    public String name;
    public String icon;
    public String type;
    public int rarity;
    public String job;
    public int cost;
    public float baseefficiency;
    public int baseresearch;
    public int baseproduction;
    public int basemaintenance;
    public Array<Passive> passives;
    public Array<Active> actives;
}
