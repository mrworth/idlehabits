package com.idlehabits.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;

/**
 * Created by aguac on 3/31/2018.
 */

public class DemoInputProcessor implements InputProcessor{

    public DemoSpriteMap demoSprites;
    private boolean dragging;

    public DemoInputProcessor(DemoSpriteMap demoSpriteMapArg){
        this.demoSprites = demoSpriteMapArg;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (button != Input.Buttons.LEFT || pointer > 0) return false;
        demoSprites.currentScreenWidth = Gdx.graphics.getWidth();
        demoSprites.currentScreenHeight = Gdx.graphics.getHeight();
        demoSprites.xReleased = demoSprites.convertXValueToScale(screenX);
        demoSprites.yReleased = demoSprites.convertYValueToScale(demoSprites.currentScreenHeight-screenY);

        System.out.println("released at X: "+demoSprites.xReleased+",Y: "+demoSprites.yReleased);
        dragging = false;
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
