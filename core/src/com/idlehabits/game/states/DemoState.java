package com.idlehabits.game.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.idlehabits.game.DemoInputProcessor;
import com.idlehabits.game.DemoSpriteMap;
import com.idlehabits.game.JSONToResource;

/**
 * Created by aguac on 3/31/2018.
 */

public class DemoState extends State{
    private Texture demoBtn;
    private Texture demoBtn2;
    private Texture demoBtn3;
    private Texture border;
    private int goldAmt;
    private int rocksAmt;
    private int sticksAmt;
    private Array craftingInventory;
    private DemoSpriteMap demoSprites = new DemoSpriteMap();
    private DemoInputProcessor demoInputs = new DemoInputProcessor(demoSprites);
    private BitmapFont demoText;
    private BitmapFont demoNum;
    private Array actionQueue;
    private Music titleMusic;
    private float musicTimer;
    private float musicTimer2;
    private boolean musicEvent;
    Sound sound = Gdx.audio.newSound(Gdx.files.internal("mysound.mp3"));

    //TODO: relegate the texture generation to demoSprites via json file parsing to get all necessary details
    //this has the benefit of letting us draw a group of sprites as a single object.
    //we could also have a designation for json objects which links a file
    //as the texture info, loading a group of textures in the same way as a texture
    //(this has benefits for loading composite images)
    //to implement this for a grouped texture holding the relative positioning in the
    //composite texture and using that compared to the render information given for the composite texture is probably best
    //example:
    //button with border and sprite
    //width of button and border should be same as composite sprite

    public DemoState(GameStateManager gsm){
        super(gsm);
        demoBtn = new Texture("playBtn.png");
        demoBtn2 = new Texture("playBtn.png");
        border = new Texture("buttonBorder.png");
        demoBtn3 = new Texture("playBtn.png");
        demoText = new BitmapFont();
        JSONToResource.parseTexture(demoSprites,"demo.json",0,0);
        demoText.setColor(Color.RED);
        demoNum = new BitmapFont();
        demoNum.setColor(Color.WHITE);
        titleMusic = Gdx.audio.newMusic(Gdx.files.internal("title.mp3"));
        titleMusic.setVolume(.01f);
        titleMusic.setLooping(true);
        titleMusic.play();
        //demoSprites.addEntry(demoBtn,1,500,450,550);
        //demoSprites.addEntry(border,3,300,320,470);
        //demoSprites.addEntry(demoBtn2,2,300,320,470);
        demoSprites.addText(demoText,"wow",70,70);
        demoSprites.addNumber(demoNum,2,90,90);
        //demoSprites.addEntryCentered(demoBtn3,4,600,600,300);
        Gdx.input.setInputProcessor(demoInputs);
        actionQueue = new Array();
        demoSprites.setActionQueue(actionQueue);

    }

    @Override
    public void handleInput(){

    }

    @Override
    public void update(float dt){
        if(actionQueue.size>0){
            int num = (Integer) actionQueue.pop();
            System.out.println(num);
            switch(num){
                case(2): {

                }
                case(4):{
                    System.out.println("yup");
                    BitmapFont bmf = demoSprites.bitmapFonts.get(1);
                    demoSprites.bitmapFontDetailsMap.get(bmf)[3]++;
                    //sound.play(1.0f);
                    //titleMusic = Gdx.audio.newMusic(Gdx.files.internal("music2.mp3"));
                    //titleMusic.play();
                    silenceMusic(titleMusic);
                    break;
                }
                default:{
                    System.out.println("event not assigned");
                    break;
                }
            }

        }
        if(musicEvent){
            musicTimer+=dt;
            if(musicTimer-musicTimer2>=1) {
                musicTimer2++;
                silenceMusic(titleMusic);
            }

        }
    }

    @Override
    public void render(SpriteBatch sb){
        demoSprites.renderSprites(sb);
    }

    @Override
    public void dispose(){
        demoSprites.disposeSprites();
    }

    public void silenceMusic(Music music) {
        if (musicEvent != true && musicTimer == 0) {
            musicEvent = true;
        }
        float volume = music.getVolume();
        volume = volume *.6f;
        System.out.println(volume);
        music.setVolume(volume);
        music.play();
        if (musicTimer >= 9) {
            music.setVolume(0);
            music.play();
            musicEvent=false;
            musicTimer=0;
            musicTimer2=0;
        }
    }


}
