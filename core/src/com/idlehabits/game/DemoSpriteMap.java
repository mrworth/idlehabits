package com.idlehabits.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;

import java.util.HashMap;


/**
 * Created by aguac on 3/31/2018.
 */

public class DemoSpriteMap{

    public HashMap<Integer,int[]> spriteDetailsMap;
    public HashMap<BitmapFont,Integer[]> bitmapFontDetailsMap;
    public HashMap<BitmapFont,String> textInfo;
    public Array<Texture> textures;
    public Array<BitmapFont> bitmapFonts;
    public int xReleased;
    public int yReleased;
    public int currentScreenHeight = DemoGame.height;
    public int currentScreenWidth = DemoGame.width;
    private Array actionQueue;


    public DemoSpriteMap(){
        spriteDetailsMap = new HashMap<Integer,int[]>();
        textures = new Array<Texture>();
        bitmapFonts = new Array<BitmapFont>();
        bitmapFontDetailsMap = new HashMap<BitmapFont, Integer[]>();
        textInfo = new HashMap<BitmapFont, String>();
        actionQueue = new Array();
    }

    public void setActionQueue(Array actionQueue){
        this.actionQueue = actionQueue;
    }

    public Array getActionQueue(){
        return this.actionQueue;
    }
    public void addEntryScaled(Texture texture,int spriteId, int spritex1, int spritey1, int spritex2, int spritey2){
      int[] spriteInfo = new int[5];

        spriteInfo[0]=spriteId;
        spriteInfo[1]=spritex1;
        spriteInfo[2]=spritey1;
        spriteInfo[3]=spritex2;
        spriteInfo[4]=spritey2;
        spriteDetailsMap.put(texture.hashCode(),spriteInfo);
        textures.add(texture);
    }
    public void addEntry(Texture texture,int spriteId,int spriteyMid,int spritex1,int spritex2){
        int[] spriteInfo = new int[5];
        int textureHeight = texture.getHeight();
        int textureWidth = texture.getWidth();
        System.out.println("height:"+textureHeight+",width:"+textureWidth+",ymid:"+spriteyMid);
        int spriteRelativeWidth = spritex2-spritex1;
        int spriteRelativeHeight = spriteRelativeWidth*textureHeight/textureWidth;
        System.out.println("relative width: "+spriteRelativeWidth+", relative height: "+spriteRelativeHeight);
        spriteInfo[0]=spriteId;
        spriteInfo[1]=spritex1;
        spriteInfo[2]=spriteyMid-spriteRelativeHeight/2;
        spriteInfo[3]=spritex2;
        spriteInfo[4]=spriteyMid+spriteRelativeHeight/2;
        System.out.println(spriteInfo[1]+","+spriteInfo[2]+","+spriteInfo[3]+","+spriteInfo[4]);
        spriteDetailsMap.put(texture.hashCode(),spriteInfo);
        textures.add(texture);
    }
    public void addEntryCentered(Texture texture,int spriteId,int spritexMid,int spriteyMid,int spriteWidth){
        int spritex1 = spritexMid - spriteWidth/2;
        int spritex2 = spritexMid + spriteWidth/2;
        addEntry(texture,spriteId,spriteyMid,spritex1,spritex2);
    }
    public void renderSprites(SpriteBatch sb){
        sb.begin();
        int screenWidth = DemoGame.width;
        int screenHeight = DemoGame.height;

        for(Texture texture : textures){
            int[] spriteInfo = spriteDetailsMap.get(texture.hashCode());
            if(xReleased+yReleased>0 && spriteInfo[0]!=0 && isPointerInRange(xReleased,yReleased,spriteInfo[1],spriteInfo[2],spriteInfo[3],spriteInfo[4])){
                System.out.println("x1:"+spriteInfo[1]+",x2:"+spriteInfo[3]);
                System.out.println("y1:"+spriteInfo[2]+",y2:"+spriteInfo[4]);
                System.out.println("ID:"+spriteInfo[0]);
                actionQueue.add(spriteInfo[0]);
                xReleased=0;
                yReleased=0;
            }
            sb.draw(texture,spriteInfo[1]*screenWidth/1000,spriteInfo[2]*screenHeight/1000,(spriteInfo[3]-spriteInfo[1])*screenWidth/1000,(spriteInfo[4]-spriteInfo[2])*screenHeight/1000);
        }

        for(BitmapFont bitmapFont : bitmapFonts){
            Integer[] bitmapFontInfo = bitmapFontDetailsMap.get(bitmapFont);
            bitmapFont.getData().setScale(1,currentScreenHeight/DemoGame.height);
            if(bitmapFontInfo[0]==1){
                String text = textInfo.get(bitmapFont);
                bitmapFont.draw(sb,text,bitmapFontInfo[1],bitmapFontInfo[2]);
            }else{
                bitmapFont.draw(sb,bitmapFontInfo[3].toString(),bitmapFontInfo[1],bitmapFontInfo[2]);
            }
        }
        sb.end();
        handleEvents();
    }

    public void addText(BitmapFont bitmapFont,String text,int xpos, int ypos){
        textInfo.put(bitmapFont,text);
        Integer[] bitMapFontInfo = new Integer[5];
        //is the bitmapfont text or number based
        bitMapFontInfo[0]=1;
        bitMapFontInfo[1]=xpos;
        bitMapFontInfo[2]=ypos;
        bitMapFontInfo[3]=-1;
        bitmapFontDetailsMap.put(bitmapFont,bitMapFontInfo);
        bitmapFonts.add(bitmapFont);
    }

    public void addNumber(BitmapFont bitmapFont,int num,int xpos,int ypos){
        Integer[] bitMapFontInfo = new Integer[5];
        //is the bitmapfont text or number based
        bitMapFontInfo[0]=0;
        bitMapFontInfo[1]=xpos;
        bitMapFontInfo[2]=ypos;
        bitMapFontInfo[3]=num;
        bitmapFontDetailsMap.put(bitmapFont,bitMapFontInfo);
        bitmapFonts.add(bitmapFont);
    }

    public void disposeSprites(){
        for(Texture texture : textures){
            texture.dispose();
        }
        for(BitmapFont bitmapFont : bitmapFonts){
            bitmapFont.dispose();
        }
        spriteDetailsMap.clear();
        bitmapFontDetailsMap.clear();
        textInfo.clear();
        textures.clear();
        bitmapFonts.clear();
    }

    public boolean isValueInRange(int num,int bottom, int top){
        return ((num-top)*(num-bottom)) <=0;
    }
    public int convertXValueToScale(int xvalue){
        return (xvalue*1000)/ currentScreenWidth;
    }
    public int convertYValueToScale(int yvalue){
        return (yvalue*1000)/ currentScreenHeight;
    }
    public boolean isPointerInRange(int xvalue, int yvalue,int x1, int y1,int x2,int y2){
        return isValueInRange(xvalue,x1,x2) && isValueInRange(yvalue,y1,y2);
    }

    public void handleEvents(){
        if(actionQueue!=null && actionQueue.size>0) {

        }
    }

}
