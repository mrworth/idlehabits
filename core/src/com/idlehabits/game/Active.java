package com.idlehabits.game;

/**
 * Created by aguac on 4/5/2018.
 */

public class Active {
    public int level;
    public int id;
    public String name;
    public String icon;
    public int cooldown;
    public String effect;
}
