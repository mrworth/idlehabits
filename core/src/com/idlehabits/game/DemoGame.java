package com.idlehabits.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.idlehabits.game.states.DemoState;
import com.idlehabits.game.states.GameStateManager;

/**
 * Created by aguac on 3/30/2018.
 */

public class DemoGame extends ApplicationAdapter{
    public static final int width= 480;
    public static final int height = 800;
    public static final String title = "Demo Title";
    private GameStateManager gsm;
    private SpriteBatch batch;

    @Override
    public void create(){
        batch = new SpriteBatch();
        gsm = new GameStateManager();
        Gdx.gl.glClearColor(1,0,0,1);
        gsm.push(new DemoState(gsm));
    }

    @Override
    public void render(){
        gsm.update(Gdx.graphics.getDeltaTime());
        gsm.render(batch);
    }
}
