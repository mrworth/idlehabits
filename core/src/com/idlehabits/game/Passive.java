package com.idlehabits.game;

/**
 * Created by aguac on 4/5/2018.
 */

public class Passive {
    public int level;
    public int id;
    public String name;
    public String icon;
    public boolean toggle;
    public String effect;
}
