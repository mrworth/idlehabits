package com.idlehabits.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.idlehabits.game.DemoGame;

/**
 * Created by aguac on 3/30/2018.
 */

public class DemoLauncher {
    public static void main (String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width = DemoGame.width;
        config.height = DemoGame.height;
        config.title = DemoGame.title;
        new LwjglApplication(new DemoGame(), config);

    }
}
