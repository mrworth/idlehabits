package com.idlehabits.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.idlehabits.game.*;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = IdleHabits.width;
		config.height = IdleHabits.height;
		config.title = IdleHabits.title;
		new LwjglApplication(new IdleHabits(), config);

	}
}
