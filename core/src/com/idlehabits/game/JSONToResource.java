package com.idlehabits.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;

/**
 * Created by aguac on 4/5/2018.
 */

public class JSONToResource {
    public static void parseTexture(DemoSpriteMap spriteMap,String fileName,int xposBase, int yposBase) {
        JsonReader reader = new JsonReader();
        JsonValue root = reader.parse(Gdx.files.internal(fileName).readString());
        for (int i = 0; i < root.size; i++) {
            JsonValue element = root.get(i);
            JsonValue isToggleSet = element.get("toggleoff");
            JsonValue isScaledSet = element.get("scaled");
            if (isToggleSet == null || isToggleSet.asString().equals("no")) {
                if (isScaledSet == null || isScaledSet.asString().equals("no")) {
                    String image = element.get("image").asString();
                    int id = element.get("id").asInt();
                    int xpos = element.get("xpos").asInt();
                    int ypos = element.get("ypos").asInt();
                    int width = element.get("width").asInt();
                    spriteMap.addEntryCentered(new Texture(image), id, xpos, ypos, width);
                    if(element.get("bundle")!=null){
                        parseBundleCentered(spriteMap,element.get("bundle"),xpos,ypos);
                    }
                    if(element.get("fileimages")!=null){
                        for(String subFileName : element.get("fileimages").asStringArray()){
                            JsonValue subRoot = reader.parse(Gdx.files.internal(subFileName).readString());
                            System.out.println("subroot" +subRoot);
                            parseBundleCentered(spriteMap,subRoot,xpos,ypos);
                        }
                    }
                } else {
                    String image = element.get("image").asString();
                    int id = element.get("id").asInt();
                    int xpos1 = element.get("xpos1").asInt();
                    int ypos1 = element.get("ypos1").asInt();
                    int xpos2 = element.get("xpos2").asInt();
                    int ypos2 = element.get("ypos2").asInt();
                    spriteMap.addEntryScaled(new Texture(image), id, xpos1, ypos1, xpos2, ypos2);

                }
            }
        }
    }

    public static void parseBundleCentered(DemoSpriteMap spriteMap,JsonValue jsonValue,int xposBase,int yposBase){
        JsonReader reader = new JsonReader();
        for(int i=0;i<jsonValue.size;i++){
            JsonValue element = jsonValue.get(i);
            String image = element.get("image").asString();
            int id = element.get("id").asInt();
            int xpos = xposBase+element.get("xshift").asInt();
            int ypos = yposBase+element.get("yshift").asInt();
            int width = element.get("width").asInt();
            spriteMap.addEntryCentered(new Texture(image), id, xpos, ypos, width);
            if(element.get("bundle")!=null){
                parseBundleCentered(spriteMap,element.get("bundle"),xpos,ypos);
            }
            if(element.get("fileimages")!=null){
                for(String subFileName : element.get("fileimages").asStringArray()){
                    JsonValue subRoot = reader.parse(Gdx.files.internal(subFileName).readString());
                    System.out.println("subroot" +subRoot);
                    parseBundleCentered(spriteMap,subRoot,xpos,ypos);
                }
            }
        }
    }

    public static Array<NPC> parseNPCs(String fileName){
        return null;
    }
}
